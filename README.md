# Variant Calling Pipeline POC for DNANexus

## The pipeline is divided into following stages:

### Stage 0a. Sort BAM with read names.

### Stage 0b. Roll back and map reads to reference genome.

### Stage 1a. Sort the BAM files and mark the duplicate reads.

### Stage 1b. Check data quality of the mapping (pre-VCF check)

### Stage 2a. Recalibrate and check quality of the BAMs.

### Stage 2b. Individual SNV genotyping calling using GATK.

### Stage 2c. Project level VCF
