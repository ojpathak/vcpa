###############################################################################
#
# Stage 0a. Sort BAM with read names 
# Inputs:
# Output:
# Usage:
#
###############################################################################

SAMBAMBA="sambamba"
THREADS=""
RG=""
SORTED_OUTPUT_BAM=""
SEQFILE=""
TMP_DIR="${PWD}/tmp"

usage(){
	echo " s0a.sh [-t THREADS -r READGROUP] -i INPUT_BAM -o SORTED_OUTPUT_BAM"
	exit 1
}

# sort the bam by name(queryname) while filtering out read groups into a new bam
# @param $INPUT_BAM - input BAM
# @param $RG      - read-group for filtering by
# @return SORTED_OUTPUT_BAM - single read-group BAM sorted by name

while getopts t:r:i:o:h flag
do
    case "${flag}" in
        t) THREADS=${OPTARG};;
        r) RG=${OPTARG};;
        i) INPUT_BAM=${OPTARG};;
        o) SORTED_OUTPUT_BAM=${OPTARG};;
        h) usage;;
    esac
done

echo ""
echo "Beginning Stage 0a ..."
echo ""

if [ -d "$TMP_DIR" ]
then
	rm -rf $TMP_DIR
	mkdir $TMP_DIR
fi

if [ -z "$THREADS" ]
then
	NTHREADS=""
else
	NTHREADS="--nthreads ${THREADS}"
fi

if [ -z "$RG" ]
then
	FILTER=""
else
	FILTER="--filter [RG]=="\'${RG}\'
fi

echo ""
echo "Executing command: $SAMBAMBA sort --tmpdir=$TMP_DIR $NTHREADS
		--sort-by-name $FILTER --compression-level=8 --out $SORTED_OUTPUT_BAM
		$INPUT_BAM"

$SAMBAMBA sort \
	--tmpdir=$TMP_DIR \
	$NTHREADS \
	--sort-by-name \
	$FILTER \
	--compression-level=8 \
	--out $SORTED_OUTPUT_BAM \
	$INPUT_BAM

echo ""
echo "Completed Stage 0a ..."
echo ""
