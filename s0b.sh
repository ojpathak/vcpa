###############################################################################
#
# Stage 0b. Roll back and map reads to reference genome.
# Inputs:
# Output:
# Usage:
#
###############################################################################

DIR="."
OUT_DIR="."
BWA="bwa"
SAMTOOLS="samtools"
PICARD="/mnt/data/NGS/jar/picard-git/build/libs/picard.jar"
TMP_DIR_R="${PWD}/tmp"
REF_FASTA="/mnt/data/NGS/ref/hg38/GRCh38_full_analysis_set_plus_decoy_hla.fa"
LOGS="${TMP_DIR_R}/logs"

usage(){
	echo " s0b.sh [-t THREADS -r RG] -i INPUT_BAM -o ALIGNED_OUTPUT_BAM"
	exit 1
}

while getopts t:r:i:o:h flag
do
    case "${flag}" in
        t) THREADS=${OPTARG};;
        r) RG=${OPTARG};;
        i) INPUT_BAM=${OPTARG};;
        o) ALIGNED_OUTPUT_BAM=${OPTARG};;
        h) usage;;
    esac
done


echo ""
echo "Beginning Stage 0b ..."
echo ""

if [ -d "$TMP_DIR_R" ]
then
	rm -rf $TMP_DIR_R
	mkdir $TMP_DIR_R
	mkdir $TMP_DIR_R/logs
fi

# uses SamToFastq to convert input BAM to FASTQ in stdout
# piped to BWA for mapping, output piped to split-sam-stream for
# splitting by chr
# @param SEQFILE - input BAM, single read-group, sorted by name, using original qualities
# @param THREADS - # of threads for BWA
# Output - NM.aligned is a mapped bam file (note: per read group per chromosome)  

RG_LINE=$($SAMTOOLS view -H ${INPUT_BAM} |grep  "^@RG" | grep -m 1 -P "ID:${RG}\s" |sed 's/\t/\\t/g')
echo $RG_LINE

java -Xmx500M -jar $PICARD RevertSam \
	TMP_DIR=$TMP_DIR_R \
	OUTPUT_BY_READGROUP=false \
	RESTORE_ORIGINAL_QUALITIES=true \
	SORT_ORDER=queryname \
	COMPRESSION_LEVEL=0 \
	VALIDATION_STRINGENCY=SILENT \
	QUIET=true \
	INPUT=$INPUT_BAM \
	OUTPUT=/dev/stdout | \
		java -Xmx500M -jar $PICARD MarkIlluminaAdapters \
			TMP_DIR=$TMP_DIR_R \
			METRICS=$LOGS/$RG.metrics \
			COMPRESSION_LEVEL=0 \
			VALIDATION_STRINGENCY=SILENT \
			QUIET=true \
			INPUT=/dev/stdin \
			OUTPUT=/dev/stdout | \
				java -Xmx2G -jar $PICARD SamToFastq  \
					CLIPPING_ATTRIBUTE=XT \
					CLIPPING_ACTION=2 \
					INTERLEAVE=true \
					INCLUDE_NON_PF_READS=true \
					INCLUDE_NON_PRIMARY_ALIGNMENTS=false \
					TMP_DIR=$TMP_DIR_R \
					COMPRESSION_LEVEL=0 \
					VALIDATION_STRINGENCY=SILENT \
					QUIET=true \
					INPUT=/dev/stdin \
					FASTQ=/dev/stdout | \
						$BWA mem \
							-t $THREADS \
							-K 100000000 \
							-Y \
							-R "$RG_LINE" \
							-p $REF_FASTA - | \
								$DIR/split-sam-stream.pl $OUTDIR/${RG}.aligned

echo ""
echo "Completed Stage 0b ..."
echo ""
