###############################################################################
#
# Stage 1a. Sort the BAM files and mark the duplicate reads.
# Inputs:
# Output:
# Usage:
#
###############################################################################

SAMTOOLS="samtools"
SAMBLASTER="/mnt/data/NGS/bin/samblaster"
SAMBAMBA="sambamba"
BAMUTIL="/mnt/data/NGS/bin/bamUtil-git/bin/bam"

usage(){
	echo " s1a.sh [-c CHR -r READGROUP] -i INPUT_SAM -o OUTPUT_SORTED_DUPMARKED_BAM"
	exit 1
}

while getopts r:c:i:o:h flag
do
    case "${flag}" in
        r) RG=${OPTARG};;
        c) CHR=${OPTARG};;
        i) INPUT_SAM=${OPTARG};;
        o) OUTPUT_SORTED_DUPMARKED_BAM=${OPTARG};;
        h) usage;;
    esac
done

echo ""
echo "Beginning Stage 1a ..."
echo ""

# sorts the input file by query-name using samtools,
# streammed to samblaster for adding in MC/MQ tags
# output coordinate sorted BAM with MC/MQ tags
# @param SAM - input SAM file
# @output BAM - coordinate-sorted BAM

$SAMTOOLS sort --threads 2 -l 0 -n -T "${RG}.aligned.${CHR}.sorted.bam.1" --output-fmt SAM "$INPUT_SAM" | \
       $SAMBLASTER --addMateTags --ignoreUnmated | \
$SAMTOOLS sort -@2 -l 5 -T "${RG}.aligned.${CHR}.sorted.bam.2" --output-fmt BAM -o "${RG}.aligned.${CHR}.sorted.bam"

###############################################################################
# Mark duplicates
###############################################################################

# @param (-d) DODEBUG=true - not used
# @param (-r RGBAM) - full read-group bam path include BAM_DIR, used to grab read group list
# @param (-o MRGBAM) - merge bam name (output bam)

$SAMBAMBA merge --compression-level=9 ${RG}.merged.bam ${RG}.aligned.${CHR}.sorted.bam
$BAMUTIL dedup_lowmem --in ${RG}.merged.bam --out ${RG}.sorted.dupmarked.bam \
	--force --excludeFlags 0xB00 && rm -fv ${RG}.merged.bam && \
	$SAMBAMBA index ${RG}.sorted.dupmarked.bam

echo ""
echo "Completed Stage 1a ..."
echo ""
