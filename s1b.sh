###############################################################################
#
# Stage 1b. Check data quality of the mapping (pre-VCF check)
# Inputs:
# Output:
# Usage:
#
###############################################################################

SAMTOOLS="samtools"
SAMBAMBA="sambamba"
VERIFYBAMID="verifyBamID"

usage(){
	echo " s1b.sh -c CHR -i INPUT_BAM -o OUTFILE"
	exit 1
}

while getopts c:i:o:h flag
do
    case "${flag}" in
        c) CHR=${OPTARG};;
        i) INPUT_BAM=${OPTARG};;
        o) OUTFILE=${OPTARG};;
        h) usage;;
    esac
done

echo ""
echo "Beginning Stage 1b ..."
echo ""

# Sambamba flagstats

# 5x, 10x, 20x, 30x, 40x, 50x coverage
$SAMBAMBA depth region \
	-t 2 \
	--combined \
	-L ${CHR} \
	-T 5 -T 10 -T 20 -T 30 -T 40 -T 50 ${INPUT_BAM} >> $OUTFILE

# Overall average coverage
#AVGCOV=$(grep "^chr" $INFILE | awk -v RSIZE=$RSIZE '{SUM += $4 * RSIZE / $3} END{ printf "%0.2f", SUM/NR }')

#where INFILE = variant eval output; RSIZE = mapped read length


# Check sex based on the difference between X and Y chromosome coverage.
# Check coverage

#$SAMTOOLS depth -q30 -Q37 -a -b Homo_sapiens_assembly38.dbsnp138.vcf.bed [BAMFILE] > [OUTFILE]

# Calculate chromosomal coverage (Perl code)

# Check X-Y chromosome coverage to impute sex (R code)

# Check concordance of BAM with GWAS data

#$VERIFYBAMID --vcf [GWAS_VCF] --bam [BAMFILE] --out [OUTFILE] --verbose —ignoreRG --self --best

echo ""
echo "Completed Stage 1b ..."
echo ""
