###############################################################################
#
# Stage 2a. Recalibrate and check quality of the BAMs 
# Inputs:
# Output:
# Usage:
#
###############################################################################

TMP="${PWD}/tmp"
LOGS="${TMP}/logs"
SAMTOOLS="samtools"
SAMBAMBA="sambamba"
INDEL_REALIGNER="indelrealigner.sh"
GATK3="/mnt/data/NGS/jar/gatk-3.7/GenomeAnalysisTK.jar"
REF_FASTA="/mnt/data/NGS/ref/hg38/GRCh38_full_analysis_set_plus_decoy_hla.fa"
DBSNP="/mnt/data/NGS/ref/hg38/Homo_sapiens_assembly38.dbsnp138.vcf.gz"
KNOWN="/mnt/data/NGS/ref/hg38/Homo_sapiens_assembly38.known_indels.vcf.gz"
GOLD="/mnt/data/NGS/ref/hg38/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz"

usage(){
	echo " s2a.sh [-t THREADS -r READGROUP] -i INPUT_BAM -o OUTPUT_CRAM"
	exit 1
}

while getopts t:r:i:o:h flag
do
    case "${flag}" in
        t) THREADS=${OPTARG};;
        r) RG=${OPTARG};;
        i) INPUT_BAM=${OPTARG};;
        o) OUTPUT_CRAM=${OPTARG};;
        h) usage;;
    esac
done

echo ""
echo "Beginning Stage 2a ..."
echo ""

if [ -d "$TMP" ]
then
	rm -rf $TMP
	mkdir $TMP
	mkdir $LOGS
fi

# Re-calibrate base quality scores (GATK BaseRecalibrator)
# $KNOWN: dbSNP 138 lifted Indel VCF file (specified on page 2)
# $DBSNP: dbSNP 138 lifted SNV VCF file (specified on page 2)
# $GOLD: Mills and 1000G gold standard indels (specified on page 2)

java -Djava.io.tmpdir=$TMP -jar $GATK3 \
	-T BaseRecalibrator \
	-R $REF_FASTA \
	-I $INPUT_BAM \
	-nct $THREADS \
	--useOriginalQualities \
	-knownSites $KNOWN \
	-knownSites $DBSNP  \
	-knownSites $GOLD  \
	–o ${INPUT_BAM}.recal_data.table

# Realignment reads around indels (GATK RealignTargetCreator and IndelRealigner)

echo ""
echo "Beginning Stage 2a:RealignTargetCreator ..."
echo ""

java -Djava.io.tmpdir=$TMP \
	-jar $GATK3 \
	-T RealignerTargetCreator\
	-R $REF_FASTA \
	-I ${INPUT_BAM}.recal_data.table \
	-nt $THREADS  \
	--known $KNOWN \
	--known $DBSNP \
	--known $GOLD \
	-o "${INPUT_BAM}.realigned"

echo ""
echo "Beginning Stage 2a:IndelRealigner ..."
echo ""

java -Xmx12g -Djava.io.tmpdir=${TMP} \
          -jar $GATK3 \
          -T IndelRealigner\
          -R $REF_FASTA \
          -I ${INPUT_BAM}.realigned \
          -known $KNOWN \
          -known $DBSNP \
          -known $GOLD \
          --targetIntervals ${INPUT_BAM}.indels.intervals \
          -BQSR ${INPUT_BAM}.recal_data.table \
          --bam_compression 1 \
          -kpr \
          -SQQ 10 -SQQ 20 -SQQ 30 -SQQ 40 \
          --disable_indel_quals \
          --preserve_qscores_less_than 6 \
          -log $LOGS/indel.log \
          -o ${INPUT_BAM}.indelrealign.bam


echo ""
echo "Beginning Stage 2a:Merging ..."
echo ""
# merging all the mapped and unmapped reads (chr 1-22, X,Y,MT,other decoys, -unmapped) into a file
$SAMBAMBA merge --nthreads=$THREADS --compression-level=8 ${INPUT_BAM}.hg38.realign.bqsr.bam

# saving a CRAM file using the following command
CRAMFILE="${INPUT_BAM}.hg38.realign.bqsr.cram"
$SAMTOOLS view -C -T $REF_FASTA -@$THREADS -o $CRAMFILE ${INPUT_BAM}.hg38.realign.bqsr.bam

#Generate md5sum files for BAM and CRAM
md5sum ${INPUT_BAM}.hg38.realign.bqsr.bam > ${INPUT_BAM}.hg38.realign.bqsr.bam.md5sum
md5sum $CRAMFILE > $CRAMFILE.md5sum

echo ""
echo "Completed Stage 2a ..."
echo ""
