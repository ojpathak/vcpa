###############################################################################
#
# Stage 2b. Individual SNV genotyping calling using GATK 
# Inputs:
# Output:
# Usage:
#
###############################################################################
XMEM="4G"
TMP="${PWD}/tmp"
LOGS="${TMP}/logs"
GATK4="/mnt/data/NGS/jar/gatk-4.1.1.0/gatk-package-4.1.1.0-local.jar"
REF_FASTA="/mnt/data/NGS/ref/hg38/GRCh38_full_analysis_set_plus_decoy_hla.fa"
DBSNP="/mnt/data/NGS/ref/hg38/Homo_sapiens_assembly38.dbsnp138.vcf.gz"

usage(){
	echo " s2b.sh [-t THREADS -r READGROUP -c CHR] -i INPUT_BAM -o OUTPUT_VCF"
	exit 1
}

while getopts t:r:c:i:o:h flag
do
    case "${flag}" in
        t) THREADS=${OPTARG};;
        r) RG=${OPTARG};;
        c) CHR=${OPTARG};;
        i) INPUT_BAM=${OPTARG};;
        o) OUTPUT_VCF=${OPTARG};;
        h) usage;;
    esac
done

echo ""
echo "Beginning Stage 2b ..."
echo ""

# run GATK HaplotypeCaller on a region of input bam to generate a GVCF
# @param (-i INPUTBAM) - input SEQ FILE
# @param (-c CHR)      - region for "-L" interval flag
# @param (-p PREFIX)   - out-file naming prefix
# @param (-n)          - override PCRMODEL from CONSERVATIVE to NONE, required for PCR-free data
# @return OUTFILE - named as ${PREFIX}.$CHR.g.vcf.gz

java -Xmx$XMEM -Djava.io.tmpdir=$TMP \
	-jar $GATK4 \
	-T HaplotypeCaller \
	-R $REF_FASTA \
	-I "$INPUT_BAM" \
	-nct $THREADS \
	-L $CHR  \
	--dbsnp $DBSNP \
	--genotyping_mode DISCOVERY \
	--minPruning 2 \
	-newQual \
	-stand_call_conf 30 \
	--emitRefConfidence GVCF \
	--pcr_indel_model "CONSERVATIVE" \
	-l INFO \
	-log $LOGS/vch.$CHR.log \
	-o "${INPUT_BAM}.$CHR.g.vcf.gz"

echo ""
echo "Completed Stage 2b ..."
echo ""
