###############################################################################
#
# Stage 2c. Project level VCF 
# Inputs:
# Output:
# Usage:
#
###############################################################################
TMP="${PWD}/tmp"
BCFTOOLS="bcftools"
GATK4="/mnt/data/NGS/jar/gatk-4.1.1.0/gatk-package-4.1.1.0-local.jar"
HAPMAP="/mnt/data/NGS/ref/hg38/hapmap_3.3.hg38.vcf.gz"
OMNI="/mnt/data/NGS/ref/hg38/1000G_omni2.5.hg38.vcf.gz"
HIGH="/mnt/adsp/1000G_phase1.snps.high_confidence.hg38.vcf.gz"
DBSNP="/mnt/data/NGS/ref/hg38/Homo_sapiens_assembly38.dbsnp138.vcf.gz"
REF_FASTA="/mnt/data/NGS/ref/hg38/GRCh38_full_analysis_set_plus_decoy_hla.fa"

usage(){
	echo " s2c.sh [-t THREADS -c CHR] -i INPUT_VCF -o OUTPUT_VCF"
	exit 1
}

while getopts t:c:i:o:h flag
do
    case "${flag}" in
        t) THREADS=${OPTARG};;
        c) CHR=${OPTARG};;
        i) INPUT_VCF=${OPTARG};;
        o) OUTPUT_VCF=${OPTARG};;
        h) usage;;
    esac
done

echo ""
echo "Beginning Stage 2c ..."
echo ""

if [ -d "$TMP" ]
then
	rm -rf $TMP
	mkdir $TMP
fi

# Combine multiple gVCFs combine all the sample level gVCFs using the combinegVCFs function.

java -Djava.io.tmpdir=$TMP -jar $GATK4 \
	-T GenotypeGVCFs \
	-R $REF_FASTA \
	--dbsnp $DBSNP \
	-nt $THREADS \
	-L chr$CHR \
	--variant $INPUT_VCF \
	-o "$OUTFILE"

# GenotypedGVCF(GATK4.1.1): joint genotype calling on the output from previous step
# Variant Quality Score Recalibration (VQSR) on both SNPs and indels (GATK4.1.1)
java -Xmx440g -Djava.io.tmpdir=$TMP -jar $GATK4 \
	-T VariantRecalibrator \
	-R $REF_FASTA \
	-L chr1 -L chr2 -L chr3 -L chr4 -L chr5 \
	-L chr6 -L chr7 -L chr8 -L chr9 -L chr10 \
	-L chr11 -L chr12 -L chr13 -L chr14 -L chr15 \
	-L chr16 -L chr17 -L chr18 -L chr19 -L chr20 \
	-L chr21 -L chr22 -L chrX -L chrY -L chrM \
	-resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $DBSNP \
	-resource:hapmap,known=false,training=true,truth=true,prior=15.0 $HAPMAP \
	-resource:omni,known=false,training=true,truth=true,prior=12.0 $OMNI \
	-resource:1000G,known=false,training=true,truth=false,prior=10.0 $HIGH \
	-an QD \
	-an FS \
	-an DP \
	-an SOR \
	-an MQ \
	-an ReadPosRankSum \
	-an MQRankSum \
	-an InbreedingCoeff \
	-mode SNP \
	-allPoly \
	-tranche 100.0 \
	-tranche 99.9 \
	-tranche 99.8 \
	-tranche 99.7 \
	-tranche 99.5 \
	-tranche 99.3 \
	-tranche 99.0 \
	-tranche 98.5 \
	-tranche 98.0 \
	-tranche 97.0 \
	-tranche 95.0 \
	-tranche 90.0 \
	--input $CHR \
	-nt $THREADS \
	-recalFile $PWD/*.hg38.recalibrate_SNP.recal \
	-tranchesFile $PWD/*.hg38.recalibrate_SNP.tranches \
	-rscriptFile $PWD/*.hg38.recal.recalibrate_SNP_plots.R

#VQSR INDEL
java -Xmx430g -Djava.io.tmpdir=$TMP -jar $GATK4 \
	-T VariantRecalibrator \
	-R $REF_FASTA \
	-L chr1 -L chr2 -L chr3 -L chr4 -L chr5 \
	-L chr6 -L chr7 -L chr8 -L chr9 -L chr10 \
	-L chr11 -L chr12 -L chr13 -L chr14 -L chr15 \
	-L chr16 -L chr17 -L chr18 -L chr19 -L chr20 \
	-L chr21 -L chr22 -L chrX -L chrY -L chrM \
	--maxGaussians 4 \
	-resource:mills,known=false,training=true,truth=true,prior=12.0 $GOLD \
	-resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $DBSNP \
	-an QD \
	-an FS \
	-an DP \
	-an SOR \
	-an ReadPosRankSum \
	-an MQRankSum \
	-an InbreedingCoeff \
	-mode INDEL \
	-allPoly \
	-tranche 100.0 \
	-tranche 99.9 \
	-tranche 99.8 \
	-tranche 99.7 \
	-tranche 99.5 \
	-tranche 99.3 \
	-tranche 99.0 \
	-tranche 98.5 \
	-tranche 98.0 \
	-tranche 97.0 \
	-tranche 95.0 \
	-tranche 90.0 \
	--input $CHR \
	-nt $THREADS \
	-recalFile $PWD/*.hg38.recalibrate_INDEL.recal \
	-tranchesFile $PWD/*.hg38.recalibrate_INDEL.tranches \
	-rscriptFile $PWD/*.hg38.recal.recalibrate_INDEL_plots.R

# ApplyRecalibration(GATK4.1.1): applying the desired level of recalibration to the
# SNPs and indels in the call set
if [ -d "${PWD}/new_results" ]
then
	rm -rf ${PWD}/new_results
	mkdir ${PWD}/new_results
fi

java -Xmx20g -Djava.io.tmpdir=$TMP -jar $GATK4 \
	-T ApplyRecalibration \
	-R $REF_FASTA \
	$VAR \
	$L_ARG \
	-mode SNP \
	-nt $THREADS \
	--ts_filter_level 99.0  \
	-recalFile new_results/$INPUT_VCF.hg38.tileDB.recalibrate_SNP.recal \
	-tranchesFile new_results/$INPUT_VCF.hg38.tileDB.recalibrate_SNP.tranches \
	-o new_results/$INPUT_VCF.hg38.tileDB.recalibrate_SNP.$CHR.g.vcf.bgz

# Separate the Biallelic and Multiallelic variants(bcftools)
# QC will handle biallelic and multiallelic variants separately.

$BCFTOOLS view -m2 -M2 \
	-v snps $INPUT_VCF.hg38.recalibrate_SNP.$CHR.g.vcf.bgz -Oz > \
	$INPUT_VCF.hg38.recalibrate_SNP.$CHR.biallelic.g.vcf.bgz  && \
	tabix -f $INPUT_VCF.hg38.recalibrate_SNP.$CHR.biallelic.g.vcf.bgz

echo ""
echo "Completed  Stage 2c ..."
echo ""
