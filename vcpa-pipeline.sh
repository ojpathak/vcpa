#!/bin/bash

###############################################################################
#
# This is the entry point for GATK pipeline. This script enlists all the steps
# used as part of the pipeline and calls individual scripts for each step.
# Inputs:
# Output:
# Usage:
#
###############################################################################

echo ""
echo "Beginning the GCAD WGS GATK SNV Genotype calling pipeline ..."
echo ""

###############################################################################
#
# Stage 0a. Sort BAM with read names 
# Inputs:
# Output:
# Usage:
#
###############################################################################

bash s0a.sh [-t THREADS -r READGROUP] -i INPUT_BAM -o SORTED_OUTPUT_BAM

###############################################################################
#
# Stage 0b. Roll back and map reads to reference genome.
# Inputs:
# Output:
# Usage:
#
###############################################################################

bash s0b.sh [-t THREADS -r RG] -i INPUT_BAM -o ALIGNED_OUTPUT_BAM

###############################################################################
#
# Stage 1a. Sort the BAM files and mark the duplicate reads.
# Inputs:
# Output:
# Usage:
#
###############################################################################

bash s1a.sh [-c CHR -r READGROUP] -i INPUT_SAM -o OUTPUT_SORTED_DUPMARKED_BAM

###############################################################################
#
# Stage 1b. Check data quality of the mapping (pre-VCF check)
# Inputs:
# Output:
# Usage:
#
###############################################################################

bash s1b.sh -c CHR -i INPUT_BAM -o OUTFILE

###############################################################################
#
# Stage 2a. Recalibrate and check quality of the BAMs 
# Inputs:
# Output:
# Usage:
#
###############################################################################

bash s2a.sh

###############################################################################
#
# Stage 2b. Individual SNV genotyping calling using GATK 
# Inputs:
# Output:
# Usage:
#
###############################################################################

bash s2b.sh

###############################################################################
#
# Stage 2c. Project level VCF 
# Inputs:
# Output:
# Usage:
#
###############################################################################

bash s2c.sh -t THREADS -c CHR -i INPUT_VCF -o OUTPUT_VCF

echo ""
echo "Pipeline finished."
echo ""
